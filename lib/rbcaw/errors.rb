module RbCAW
  class RequestError < StandardError
  end

  class ModelError < StandardError
  end

  class ResourceError < StandardError
  end

  class TimeoutError < StandardError
  end

  class ArgumentError < StandardError
  end
end
