module RbCAW
  class Cursor
    attr_reader :limit, :after, :before, :order

    def initialize(limit, after, before, order)
      @limit = limit
      @after = after
      @before = before
      @order = order
    end
  end
end
