require_relative '../../models/search/cursor'

module RbCAW
  class SearchResult
    attr_reader :results, :cursor
  end
end
