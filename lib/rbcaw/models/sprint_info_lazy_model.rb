require_relative '../models/conduit_lazy_model'

module RbCAW
  class SprintInfoLazyModel < ConduitLazyModel
    attr_reader :id
    def initialize(id, conduit: nil)
      @fetched = false

      @id = id
      @conduit = conduit
    end

    def manual_init(point, is_closed)
      @point = point
      @is_closed = is_closed

      @fetched = true
    end

    def check_fetched
      unless @fetched
        if @conduit == nil
          raise ModelError, 'Conduit helper is nil'
        end

        begin
          fetch
        rescue ResourceError => e
          raise ModelError, "Failed to fetch information for #{self.class} with id #{id}: #{e.message}"
        end
      end
    end

    def fetch
      sprint_info = @conduit.sprint.info(@id)

      manual_init(sprint_info.point, sprint_info.is_closed)
    end

    def point
      check_fetched
      return @point
    end

    def is_closed
      check_fetched
      return @is_closed
    end
  end
end
