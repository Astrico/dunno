module RbCAW
  class ConduitLazyModel
    attr_reader :phid
    def initialize(phid, conduit: nil)
      @fetched = false

      @phid = phid
      @conduit = conduit
    end

    def check_fetched
      unless @fetched
        if @conduit == nil
          raise ModelError, 'Conduit helper is nil'
        end

        begin
          fetch
        rescue ResourceError => e
          raise ModelError, "Failed to fetch information for #{self.class} with PHID #{phid}: #{e.message}"
        end
      end
    end

    def manual_init
      raise NoMethodError, 'Initialization method not defined'
    end

    def fetch
      raise NoMethodError, 'Fetch method not defined'
    end

    def ==(o)
      unless o.is_a? ConduitLazyModel
        return false
      end

      check_fetched
      o.check_fetched

      self.instance_variables.each { |var|
        if var == :@conduit
          next
        end

        unless (self.instance_variable_get(var).eql? o.instance_variable_get(var)) ||
            (self.instance_variable_get(var) == o.instance_variable_get(var))
          return false
        end
      }

      return true
    end
  end
end