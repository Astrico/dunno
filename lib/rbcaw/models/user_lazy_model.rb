require_relative '../models/conduit_lazy_model'

module RbCAW
  class UserLazyModel < ConduitLazyModel
    def manual_init(username, realname, image, roles)
      @username = username
      @realname = realname
      @image = image
      @roles = roles

      @fetched = true
    end

    def fetch
      user = @conduit.user.get(phid: @phid)

      manual_init(user.username, user.realname, user.image, user.roles)
    end

    def username
      check_fetched
      return @username
    end

    def realname
      check_fetched
      return @realname
    end

    def image
      check_fetched
      return @image
    end

    def roles
      check_fetched
      return @roles
    end
  end
end