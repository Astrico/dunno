require_relative '../fetchers/base_conduit_fetcher'
require_relative '../models/search/maniphest_search_result'

module RbCAW
  class ManiphestConduitFetcher < BaseConduitFetcher
    def search(query_key: 'all', constraints: {}, attachments: {},
               order: nil, before: nil, after: nil, limit: 100)
      json_obj = conduit_search(method: 'maniphest.search', query_key: query_key,
                                constraints: constraints, attachments: attachments,
                                order: order, before: before, after: after, limit: limit)

      return ManiphestSearchResult.new(@conduit, json_obj)
    end

    def get(phid:, request_projects: true)
      return super(
          phid: phid,
          attachments: (request_projects ? {'projects' => true} : {})
      )
    end
  end
end