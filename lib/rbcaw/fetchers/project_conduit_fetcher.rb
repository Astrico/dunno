require_relative '../fetchers/base_conduit_fetcher'
require_relative '../models/search/project_search_result'

module RbCAW
  class ProjectConduitFetcher < BaseConduitFetcher
    def search(query_key: 'all', constraints: {}, attachments: {},
               order: nil, before: nil, after: nil, limit: 100)
      json_obj = conduit_search(method: 'project.search', query_key: query_key,
                                constraints: constraints, attachments: attachments,
                                order: order, before: before, after: after, limit: limit)

      return ProjectSearchResult.new(@conduit, json_obj)
    end

    def get(phid:, request_members: true)
      return super(
          phid: phid,
          attachments: (request_members ? {'members' => true} : {})
      )
    end
  end
end
