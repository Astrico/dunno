require_relative '../fetchers/base_conduit_fetcher'
require_relative '../models/user_lazy_model'
require_relative '../models/search/user_search_result'

module RbCAW
  class UserConduitFetcher < BaseConduitFetcher
    def whoami(oauth_session_token: nil)
      json_obj = @conduit.request('user.whoami', session_token: oauth_session_token)

      user = UserLazyModel.new(json_obj['result']['phid'])
      user.manual_init(json_obj['result']['userName'], json_obj['result']['realName'],
                       json_obj['result']['image'], json_obj['result']['roles'])

      return user
    end

    def search(query_key: 'all', constraints: {}, attachments: {},
               order: nil, before: nil, after: nil, limit: 100)
      json_obj = conduit_search(method: 'user.search', query_key: query_key,
                                constraints: constraints, attachments: attachments,
                                order: order, before: before, after: after, limit: limit)

      return UserSearchResult.new(@conduit, json_obj)
    end

    def get(phid: nil, username: nil)
      if (phid == nil) && (username == nil)
        raise ArgumentError, 'One of PHID or username should not be nil'
      end

      constraints = {}

      if phid != nil
        constraints['phids'] = [phid]
      end

      if username != nil
        constraints['usernames'] = [username]
      end

      search_result = search(constraints: constraints, limit: 1)

      if search_result.results.length == 0
        raise ResourceError, "User with PHID \"#{phid}\" or username \"#{username}\" is not found"
      end

      return search_result.results[0]
    end
  end
end
