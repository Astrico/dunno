require_relative '../fetchers/base_conduit_fetcher'
require_relative '../models/sprint_info_lazy_model'

module RbCAW
  class SprintConduitFetcher < BaseConduitFetcher
    def info(task_id)
      params = {
          'task_id' => task_id
      }

      json_obj = @conduit.request('sprint.info', params: params)

      sprint_info = SprintInfoLazyModel.new(task_id)
      sprint_info.manual_init(
          json_obj['result']['points'].to_i,
          json_obj['result']['isClosed']
      )

      return sprint_info
    end
  end
end
