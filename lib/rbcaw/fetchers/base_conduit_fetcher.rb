module RbCAW
  class BaseConduitFetcher
    attr_reader :conduit
    def initialize(conduit)
      @conduit = conduit
    end

    def search(*)
      raise NoMethodError, 'Method search not defined'
    end

    def get(phid:, attachments: {})
      if phid == nil
        raise ArgumentError, 'PHID should not be nil'
      end

      search_result = search(
          constraints: {'phids' => [phid]},
          attachments: attachments,
          limit: 1
      )

      if search_result.results.length == 0
        raise ResourceError, "Resource with PHID \"#{phid}\" is not found"
      end

      return search_result.results[0]
    end

    protected
    def conduit_search(method: nil, query_key: 'all', constraints: {}, attachments: {},
                       order: nil, before: nil, after: nil, limit: 100)
      if method == nil
        raise ArgumentError, 'Conduit method name cannot be null'
      end

      params = {
          'queryKey' => query_key,
          'constraints' => constraints,
          'attachments' => attachments,
          'order' => order,
          'before' => before,
          'after' => after,
          'limit' => limit
      }

      return @conduit.request(method, params: params)
    end
  end
end