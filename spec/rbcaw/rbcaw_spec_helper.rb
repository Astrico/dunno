require 'rbcaw/conduit'
require 'rbcaw/models/user_lazy_model'
require 'rbcaw/models/project_lazy_model'
require 'rbcaw/models/task_lazy_model'

module RbCAWSpecHelper
  def conduit_host
    return CONDUIT['host']
  end

  def conduit
    return RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
  end

  def test_user
    user = RbCAW::UserLazyModel.new('PHID-USER-5fk6iyso3a337hlnwg6x')
    user.manual_init('unit_test', 'Unit Test Bot', nil, ['bot', 'verified', 'approved', 'activated'])

    return user
  end

  def test_project
    project = RbCAW::ProjectLazyModel.new('PHID-PROJ-es6tp6oh23oups5huwlc')
    project.manual_init(
        5, 'Unit Test Project', 'Project for unit tests. Do not delete.',
        'project', 'blue', [test_user], 'unit_test_project', 1489415804,
        1489639353, 1489338000, 1490893200, true
    )

    return project
  end

  def test_task
    task_owner = RbCAW::UserLazyModel.new('PHID-USER-lu6bbiqrki5egpdu6qvn', conduit: conduit);

    task = RbCAW::TaskLazyModel.new('PHID-TASK-jlukmmdkfivzryxhnxab', conduit: conduit)
    task.manual_init(5, 'Unit Test Task #1', task_owner, test_user, 'open', 90,
                     [test_project], 1489415969, 1489416046)
    task.sprint_info.manual_init(10, false)

    return task
  end
end