require 'rbcaw/conduit'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::Conduit, '#initialize' do
  include RbCAWSpecHelper

  it 'should initialize properly' do
    expect(conduit.host).to eql(conduit_host)

    expect(conduit.maniphest).not_to eql(nil)
    expect(conduit.project).not_to eql(nil)
    expect(conduit.sprint).not_to eql(nil)
    expect(conduit.user).not_to eql(nil)
    expect(conduit.util).not_to eql(nil)

    expect(conduit).to respond_to(:request)
  end
end