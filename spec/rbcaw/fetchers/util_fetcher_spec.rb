require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::UtilConduitFetcher, '#ping' do
  include RbCAWSpecHelper

  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'util_ping', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]
    end

    it 'should be able to request ping to Phabricator host' do
      expect(conduit.util.ping).not_to eql(nil)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should not be able to request ping to Conduit' do
      expect { conduit.util.ping }.to raise_error(RbCAW::TimeoutError)
    end
  end
end