require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::SprintConduitFetcher, '#info' do
  include RbCAWSpecHelper

  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'sprint_info', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @sprint_info = conduit.sprint.info(test_task.id)
    end

    it 'should retrieve the project\'s info properly' do
      expect(@sprint_info).to eq(test_task.sprint_info)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a TimeoutError' do
      expect { conduit.sprint.info(test_task.id) }.to raise_error(RbCAW::TimeoutError)
    end
  end
end