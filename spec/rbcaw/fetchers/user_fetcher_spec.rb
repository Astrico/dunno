require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::UserConduitFetcher, '#whoami' do
  include RbCAWSpecHelper

  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'user_whoami', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]
    end

    it 'should retrieve my info properly' do
      user = conduit.user.whoami

      expect(user.phid).to eq(test_user.phid)
      expect(user.username).to eq(test_user.username)
      expect(user.realname).to eq(test_user.realname)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a TimeoutError' do
      expect { conduit.user.whoami }.to raise_error(RbCAW::TimeoutError)
    end
  end
end

RSpec.describe RbCAW::UserConduitFetcher, '#get' do
  include RbCAWSpecHelper

  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'user_get', :record => :new_episodes
    end

    it 'should retrieve unit_test\'s info properly by PHID' do
      user = conduit.user.get(phid: test_user.phid)

      expect(user).to eq(test_user)
    end

    it 'should retrieve unit_test\'s info properly by username' do
      user = conduit.user.get(username: test_user.username)

      expect(user).to eq(test_user)
    end

    it 'should fail to retrieve unit_test\'s info when neither PHID nor username is supplied' do
      expect{ conduit.user.get }.to raise_error(RbCAW::ArgumentError)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should not be able to retrieve unit_test\' info and raised a TimeoutError' do
      expect { conduit.user.get(phid: test_user.phid) }.to raise_error(RbCAW::TimeoutError)
    end
  end
end