require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::ProjectConduitFetcher, '#get' do
  include RbCAWSpecHelper

  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'project_get', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @project = conduit.project.get(phid: test_project.phid)
    end

    it 'should retrieve the project\'s info properly' do
      expect(@project).to eq(test_project)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a TimeoutError' do
      expect { conduit.project.get(phid: test_project.phid) }.to raise_error(RbCAW::TimeoutError)
    end
  end
end