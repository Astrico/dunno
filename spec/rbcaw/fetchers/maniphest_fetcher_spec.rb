require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::ManiphestConduitFetcher, '#get' do
  include RbCAWSpecHelper

  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'maniphest_get', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @task = conduit.maniphest.get(phid: test_task.phid)
    end

    it 'should retrieve the task\'s info properly' do
      expect(@task).to eq(test_task)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a TimeoutError' do
      expect { conduit.maniphest.search }.to raise_error(RbCAW::TimeoutError)
    end
  end
end