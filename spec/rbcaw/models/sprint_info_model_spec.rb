require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::SprintInfoLazyModel do
  include RbCAWSpecHelper

  context 'with connected network' do
    before do
      VCR.insert_cassette 'project_model', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @task = RbCAW::SprintInfoLazyModel.new(test_task.id, conduit: conduit)
    end

    it 'should have all accessor usable and returning right values' do
      expect(@task.id).to eql(test_task.id)
      expect(@task.point).to eql(test_task.sprint_info.point)
      expect(@task.is_closed).to eql(test_task.sprint_info.is_closed)
    end

    after do
      VCR.eject_cassette
    end
  end
end