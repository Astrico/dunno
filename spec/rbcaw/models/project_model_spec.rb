require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::ProjectLazyModel do
  include RbCAWSpecHelper

  context 'with connected network' do
    before do
      VCR.insert_cassette 'project_model', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @project = RbCAW::ProjectLazyModel.new(test_project.phid, conduit: conduit)
    end

    it 'should have all accessor usable and returning right values' do
      expect(@project.phid).to eql(test_project.phid)
      expect(@project.id).to eql(test_project.id)
      expect(@project.name).to eql(test_project.name)
      expect(@project.description).to eql(test_project.description)
      expect(@project.icon).to eql(test_project.icon)
      expect(@project.color).to eql(test_project.color)
      expect(@project.members).to eq(test_project.members)
      expect(@project.slug).to eql(test_project.slug)
      expect(@project.date_created).to eql(test_project.date_created)
      expect(@project.date_modified).to eql(test_project.date_modified)
      expect(@project.start_date).to eql(test_project.start_date)
      expect(@project.end_date).to eql(test_project.end_date)
      expect(@project.is_sprint).to eql(test_project.is_sprint)
    end

    after do
      VCR.eject_cassette
    end
  end
end