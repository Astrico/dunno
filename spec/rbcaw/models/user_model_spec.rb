require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::UserLazyModel do
  include RbCAWSpecHelper

  context 'with connected network' do
    before do
      VCR.insert_cassette 'user_model', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @user = RbCAW::UserLazyModel.new(test_user.phid, conduit: conduit)
    end

    it 'should have all accessor usable and returning right values' do
      expect(@user.phid).to eql(test_user.phid)
      expect(@user.username).to eql(test_user.username)
      expect(@user.realname).to eql(test_user.realname)
      expect(@user.image).to eql(test_user.image)
      expect(@user.roles).to eql(test_user.roles)
    end

    after do
      VCR.eject_cassette
    end
  end
end