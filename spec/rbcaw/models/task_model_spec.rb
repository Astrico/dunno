require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::TaskLazyModel do
  include RbCAWSpecHelper

  context 'with connected network' do
    before do
      VCR.insert_cassette 'project_model', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @task = RbCAW::TaskLazyModel.new(test_task.phid, conduit: conduit)
    end

    it 'should have all accessor usable and returning right values' do
      expect(@task.phid).to eql(test_task.phid)
      expect(@task.id).to eql(test_task.id)
      expect(@task.name).to eql(test_task.name)
      expect(@task.author).to eq(test_task.author)
      expect(@task.owner).to eq(test_task.owner)
      expect(@task.status).to eql(test_task.status)
      expect(@task.priority).to eql(test_task.priority)
      expect(@task.projects).to eq(test_task.projects)
      expect(@task.date_created).to eql(test_task.date_created)
      expect(@task.date_modified).to eql(test_task.date_modified)
      expect(@task.sprint_info).to eq(test_task.sprint_info)
    end

    after do
      VCR.eject_cassette
    end
  end
end