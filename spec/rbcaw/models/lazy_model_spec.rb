require 'rbcaw/conduit'
require 'rbcaw/errors'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::ConduitLazyModel do
  include RbCAWSpecHelper

  context 'when no variables are fetched and network is disconnected' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout

      @user = RbCAW::UserLazyModel.new(test_user.phid, conduit: conduit)
    end

    it 'should have initialized phid and conduit variables' do
      expect(@user.phid).to eql(test_user.phid)
      expect(@user.instance_variable_get(:@conduit)).not_to eql(nil)
    end

    it 'should have initialized fetched variable set to false' do
      expect(@user.instance_variable_get(:@fetched)).to eql(false)
    end

    it 'should have nil as the value for the rest of its variables' do
      @user.instance_variables.each { |var|
        if (var == :@conduit) || (var == :@phid) || (var == :@fetched)
          next
        end

        expect(@user.instance_variable_get(var)).to eql(nil)
      }
    end
  end

  context 'when fetching with proper PHID and network is connected' do
    before do
      VCR.insert_cassette 'lazy_model_fetch', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @user = RbCAW::UserLazyModel.new(test_user.phid, conduit: conduit)
    end

    it 'should not raise any errors' do
      expect{ @user.check_fetched }.not_to raise_error
    end

    it 'should have fetched variable set to true' do
      @user.check_fetched

      expect(@user.instance_variable_get(:@fetched)).to eql(true)
    end

    it 'should have initialized variables' do
      @user.instance_variables.each { |var|
        if (var == :@conduit) || (var == :@phid) || (var == :@fetched)
          next
        end

        if var == :@image
          next
        end

        expect(@user.instance_variable_get(var)).not_to eql(nil)
      }
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'when fetching with proper PHID and network is disconnected' do
    before do
      stub_request(:any, /#{conduit.host}/).to_timeout
      @user = RbCAW::UserLazyModel.new(test_user.phid, conduit: conduit)
    end

    it 'should raise TimeoutError' do
      expect{ @user.check_fetched }.to raise_error(RbCAW::TimeoutError)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'when fetching with nonexistent PHID' do
    before do
      VCR.insert_cassette 'lazy_model_fetch', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @user = RbCAW::UserLazyModel.new('PHID-ThisShouldNotExists', conduit: conduit)
    end

    it 'should raise ModelError' do
      expect{ @user.check_fetched }.to raise_error(RbCAW::ModelError)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'when fetching without the Conduit request helper' do
    before do
      VCR.insert_cassette 'lazy_model_fetch', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @user = RbCAW::UserLazyModel.new(test_user.phid)
    end

    it 'should raise ModelError' do
      expect{ @user.check_fetched }.to raise_error(RbCAW::ModelError)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'when all variables are fetched and network is disconnected' do
    before do
      VCR.insert_cassette 'lazy_model_fetch', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      @user = RbCAW::UserLazyModel.new(test_user.phid, conduit: conduit)
      @user.check_fetched

      stub_request(:any, /#{conduit.host}/).to_timeout
    end

    it 'should have accessible, not-nil, variables' do
      expect(@user.username).not_to eql(nil)
      expect(@user.realname).not_to eql(nil)
      expect(@user.roles).not_to eql(nil)
    end

    after do
      VCR.eject_cassette
    end
  end
end