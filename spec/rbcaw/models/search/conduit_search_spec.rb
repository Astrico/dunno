require 'rbcaw/conduit'
require 'rbcaw/rbcaw_spec_helper'

RSpec.describe RbCAW::BaseConduitFetcher, '#conduit_search' do
  include RbCAWSpecHelper

  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'project_search', :record => :new_episodes, :match_requests_on => [:method, :uri, :body]

      constraints = {
          members: [test_user.phid]
      }

      @search_results = []

      @search_results.append( conduit.project.search(
          constraints: constraints, limit: 1)
      )

      @search_results.append( conduit.project.search(
          constraints: constraints, limit: 1, after: @search_results[0].cursor.after)
      )

      @search_results.append( conduit.project.search(
          constraints: constraints, after: @search_results[1].cursor.after)
      )
    end

    it 'should have searched successfully' do
      expect(@search_results.length).to eql(3)

      @search_results.each { |search|
        expect(search).not_to eql(nil)
      }
    end

    it 'should have proper paginations' do
      expect(@search_results[0].cursor.before).to eql(nil)
      (1..@search_results.length-1).each { |i|
        expect(@search_results[i].cursor.before).not_to eql(nil)
      }

      expect(@search_results[@search_results.length-1].cursor.after).to eql(nil)
      (0..@search_results.length-2).each { |i|
        expect(@search_results[i].cursor.after).not_to eql(nil)
      }
    end

    it 'should have different search items for each page' do
      search_items = []

      (0..@search_results.length-1).each { |i|
        search_result = @search_results[i]
        search_item = []

        search_result.results.each { |item|
          search_item.append(item.phid)
        }

        search_items.append(search_item)
      }

      (0..search_items.length-1).each { |i|
        (i+1..search_items.length-1).each { |j|
          expect(search_items[i] & search_items[j]).to eql([])
        }
      }
    end

    after do
      VCR.eject_cassette
    end
  end
end
