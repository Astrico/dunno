require_relative '../../lib/rbcaw/conduit'
class HomeController < ApplicationController
  def index
    conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

    @project_list = conduit.project.search().results
    @num_project = @project_list.length

    task_list = conduit.maniphest.search(attachments:{'projects':true}).results
    num_task = task_list.length

    @task_by_project = {}
    @project_list.each{ |project|
      @task_by_project[project.phid] = {}
      @task_by_project[project.phid]['list'] = []
      @task_by_project[project.phid]['num_complete'] = 0
      @task_by_project[project.phid]['num_incomplete'] = 0
    }

    task_list.each{ |task|
      task.projects.each{ |p_task|

        @task_by_project[p_task.phid]['list'].append(task)
        if task.status == 'resolved'
          @task_by_project[p_task.phid]['num_complete'] += 1
        else
          @task_by_project[p_task.phid]['num_incomplete'] += 1
        end

      }
    }

  end
end
