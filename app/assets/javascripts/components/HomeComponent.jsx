import React from 'react';

export default class HomeComponent extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}!</h1>;
  }
}
HomeComponent.propTypes = {
  name: React.PropTypes.string,
};
HomeComponent.defaultProps = {
  name: '',
};
